package com.example.logmicroservice.Controllers

import com.example.logmicroservice.Models.Entity.AbstractLog
import com.example.logmicroservice.Models.Entity.SystemLog
import com.example.logmicroservice.Models.Entity.UserLog
import com.example.logmicroservice.Service.LoggerService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@CrossOrigin
@RestController
@RequestMapping("/api/log")
class LogController (
        @Autowired
        private val logService: LoggerService,
        ){

    @GetMapping("{id}")
    fun getLog(@PathVariable("id") id: UUID): String? {
        println(id);
        return null;
    }

    @PostMapping("system")
    fun systemLog(@RequestBody log: SystemLog): ResponseEntity<Any>?{
        this.saveLog(log);
        return ResponseEntity.ok().build()
    }


    @PostMapping("user")
    fun systemLog(@RequestBody log: UserLog): ResponseEntity<Any>?{
        this.saveLog(log);
        return ResponseEntity.ok().build()
    }

    private fun saveLog(logObj: AbstractLog): Boolean {
        if(logObj is SystemLog) {
            this.logService.saveSystemLog(logObj);
        }else if(logObj is UserLog) {
            this.logService.saveUserLog(logObj);
        }else{
            return false;
        }

        return true
    }
}