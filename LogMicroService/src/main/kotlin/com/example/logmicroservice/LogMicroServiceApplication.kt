package com.example.logmicroservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LogMicroServiceApplication

fun main(args: Array<String>) {
    runApplication<LogMicroServiceApplication>(*args)
}
