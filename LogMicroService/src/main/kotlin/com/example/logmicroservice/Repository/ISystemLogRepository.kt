package com.example.logmicroservice.Repository

import com.example.logmicroservice.Models.Entity.SystemLog
import org.springframework.data.jpa.repository.JpaRepository
import java.util.UUID

interface ISystemLogRepository  : JpaRepository<SystemLog, UUID> {
}