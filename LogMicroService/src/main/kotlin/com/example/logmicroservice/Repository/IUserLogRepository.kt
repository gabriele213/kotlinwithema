package com.example.logmicroservice.Repository

import com.example.logmicroservice.Models.Entity.UserLog
import org.springframework.data.jpa.repository.JpaRepository
import java.util.UUID

interface IUserLogRepository : JpaRepository<UserLog, UUID> {
}