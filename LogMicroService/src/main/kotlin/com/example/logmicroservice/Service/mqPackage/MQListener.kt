package com.example.logmicroservice.Service.mqPackage

import com.example.logmicroservice.Models.Entity.RabbitMessage.RMSystemLog
import com.example.logmicroservice.Models.Entity.SystemLog
import com.example.logmicroservice.Service.LoggerService
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*

@Component
class MQListener (
        @Autowired
        private val logService: LoggerService,
) {

    @RabbitListener(queues = arrayOf(MQConfig.queueName))
    public fun systemListener(log: RMSystemLog) {
        println("Message received: " + log.toString());
        val sysLog = SystemLog(UUID.randomUUID(), log.message, log.description);
        logService.saveSystemLog(sysLog);
    }
}