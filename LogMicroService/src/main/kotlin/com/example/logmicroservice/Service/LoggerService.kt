package com.example.logmicroservice.Service

import com.example.logmicroservice.Models.Entity.AbstractLog
import com.example.logmicroservice.Models.Entity.SystemLog
import com.example.logmicroservice.Models.Entity.UserLog
import com.example.logmicroservice.Repository.ISystemLogRepository
import com.example.logmicroservice.Repository.IUserLogRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
@Transactional()
class LoggerService (
        var logSystemRepository: ISystemLogRepository,
        var logIUserLogRepository: IUserLogRepository

        ){

    fun saveSystemLog(log: SystemLog): SystemLog {
        return this.logSystemRepository.save(log)
    }

    fun saveUserLog(log: UserLog):UserLog{
        return this.logIUserLogRepository.save(log)
    }
}