package com.example.logmicroservice.Service.mqPackage

import org.springframework.amqp.core.AmqpTemplate
import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.TopicExchange
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MQConfig {
    //Retrieve from configuration file

    companion object {
        const val queueName: String = "system-queue"
        val exchangeName: String = "system-exchange"
        val routingKey: String = "system-routingKey"
    }

    //Make queue
    @Bean
    fun queue(): Queue{
        return Queue(queueName);
    }

    //Make exchange
    @Bean
    fun exchange(): TopicExchange{
        return TopicExchange(exchangeName);
    }

    //Bind queue to exchange
    @Bean
    fun binding(queue: Queue, exchange: TopicExchange): Binding {
        return BindingBuilder.bind(queue).to(exchange).with(routingKey);
    }

    //Message converter
    @Bean
    fun messageConverter(): MessageConverter{
        return Jackson2JsonMessageConverter();
    }

    //Create Rabbit mqTemplate
    @Bean
    fun template(connectionFactory: ConnectionFactory) : AmqpTemplate{
        val template: RabbitTemplate =  RabbitTemplate(connectionFactory)
        template.setMessageConverter(this.messageConverter())
        return template;
    }
}