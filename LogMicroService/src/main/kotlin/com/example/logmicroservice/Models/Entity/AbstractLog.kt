package com.example.logmicroservice.Models.Entity

import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter
import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class AbstractLog (
        @Column(nullable = false)
        @Id
        @Type(type="org.hibernate.type.PostgresUUIDType")
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "uuid2")
        open var id: UUID,
        @Column(name="message",nullable=false)
        open var message: String,
        @Column(name="description",nullable=true)
        open var description: String?
){
}