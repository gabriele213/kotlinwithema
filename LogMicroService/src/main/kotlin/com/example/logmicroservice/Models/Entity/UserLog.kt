package com.example.logmicroservice.Models.Entity

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
@Table(name="user_logs")
class UserLog (
        id: UUID,
        message: String,
        description: String,
        @Type(type="org.hibernate.type.PostgresUUIDType")
        @Column(name="user_id",columnDefinition = "uuid",nullable=false)
        var user_id: UUID,
) : AbstractLog(id,message, description) {

}