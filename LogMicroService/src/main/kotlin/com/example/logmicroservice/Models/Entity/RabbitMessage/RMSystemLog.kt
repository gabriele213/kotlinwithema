package com.example.logmicroservice.Models.Entity.RabbitMessage

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

class RMSystemLog(
    @JsonProperty("message")
    var message: String,
    @JsonProperty("description")
    var description: String? = null):Serializable {

    override fun toString(): String {
        return "SystemLog(message='$message', description=$description)"
    }
}