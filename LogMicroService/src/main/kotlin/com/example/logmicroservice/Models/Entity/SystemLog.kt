package com.example.logmicroservice.Models.Entity

import java.util.*
import javax.persistence.*

@Entity
@Table(name="system_logs")
open class SystemLog(
        id: UUID,
        message: String,
        description: String?
) : AbstractLog(id,message, description) {
}