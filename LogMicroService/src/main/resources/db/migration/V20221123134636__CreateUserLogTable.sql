CREATE TABLE User_Logs (
                           Id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
                           user_id uuid NOT NULL,
                           message varchar(255) NOT NULL,
                           description varchar(255)
)