CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE System_Logs (
                        Id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
                        message varchar(255) NOT NULL,
                        description varchar(255)
)
