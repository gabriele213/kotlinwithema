package com.example.logservice.Controllers;

import com.example.logservice.Models.Entity.SystemLog;
import com.example.logservice.Models.Entity.UserLog;
import com.example.logservice.Service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/api/log")
public class LogControllers {
    @Autowired
    public LogService logService;




    @PostMapping("system")
    public ResponseEntity<Object> systemLog(@RequestBody  SystemLog log){
        this.saveLog(log);
        return ResponseEntity.ok().build();
    }


    @PostMapping("user")
    public ResponseEntity<Object> systemLog(@RequestBody  UserLog log){
        System.out.println(log.toString());
        this.saveLog(log);
        return ResponseEntity.ok().build();
    }


    private Boolean saveLog(Object logOBj){
        if(logOBj instanceof SystemLog){
            this.logService.saveSystemLog((SystemLog) logOBj);
        }else if(logOBj instanceof UserLog){
            this.logService.saveUserLog((UserLog) logOBj);
        }else{
            return false;
        }
        return true;
    }

}
