package com.example.logservice.Models.Entity;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "system_logs")
public class SystemLog {
    @Id
    @Column(name="id")
    @GeneratedValue
    @Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name="message")
    private String message;

    @Column(name="description")
    private String description;



    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SystemLog(UUID id, String message, String description) {
        this.id = id;
        this.message = message;
        this.description = description;
    }

    public SystemLog() {
        super();
    }

    @Override
    public String toString() {
        return "SystemLog{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
