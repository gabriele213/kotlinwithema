package com.example.logservice.Models.Entity;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="user_logs")
public class UserLog {
    @Id
    @Column(name="id")
    @GeneratedValue
    @Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name="user_id")
    @Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID userId;

    @Column(name="description")
    private String description;

    @Column(name="message")
    private String message;



    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserLog() {
        super();
    }

    public UserLog(UUID userId, String description, String message) {
        this.userId = userId;
        this.description = description;
        this.message = message;
    }

    @Override
    public String toString() {
        return "UserLog{" +
                "id=" + id +
                ", userId=" + userId +
                ", description='" + description + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
