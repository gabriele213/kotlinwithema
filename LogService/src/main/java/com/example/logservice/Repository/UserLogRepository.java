package com.example.logservice.Repository;

import com.example.logservice.Models.Entity.UserLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserLogRepository extends JpaRepository<UserLog, UUID> {
}
