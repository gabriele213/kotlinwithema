package com.example.logservice.Service;

import com.example.logservice.Models.Entity.SystemLog;
import com.example.logservice.Models.Entity.UserLog;
import com.example.logservice.Repository.SystemLogRepository;
import com.example.logservice.Repository.UserLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional()
public class LogService {
    @Autowired
    public SystemLogRepository systemLogRepository;
    @Autowired
    public UserLogRepository userLogRepositor;

    public SystemLog saveSystemLog(SystemLog log){
        return this.systemLogRepository.save(log);
    }

    public UserLog saveUserLog(UserLog log){
        return this.userLogRepositor.save(log);
    }
    
}
