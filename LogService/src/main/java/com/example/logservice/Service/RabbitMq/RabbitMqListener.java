package com.example.logservice.Service.RabbitMq;

import com.example.logservice.Models.Entity.RabbitMq.Message;
import com.example.logservice.Models.Entity.SystemLog;
import com.example.logservice.Service.LogService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RabbitMqListener {
    @Autowired
    private LogService logService;

    @RabbitListener(queues=RabbitMqConfig.queueName)
    public void systemListener(Object log) {
        System.out.println("Message received: " + log.toString());
    }
}
