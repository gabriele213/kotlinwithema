package com.example.magazzinoservice.Repository;

import com.example.magazzinoservice.Entity.Products;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ProductsRepository extends JpaRepository<Products, UUID> {
}
