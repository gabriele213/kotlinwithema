package com.example.magazzinoservice.Entity;


import org.hibernate.annotations.Type;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Table(name="Products")
public class Products {
    @Id
    @Column(name="id")
    @GeneratedValue
    @Type(type="org.hibernate.type.PostgresUUIDType")

    private UUID id;

    @Column(name = "user_id")
    @Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID user_id;

    @Column(name="descrizione")
    private String descrizione;



    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getUser_id() {
        return user_id;
    }

    public void setUser_id(UUID user_id) {
        this.user_id = user_id;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public Products() {
    }


    public Products(UUID id, UUID user_id, String descrizione) {
        this.id = id;
        this.user_id = user_id;
        this.descrizione = descrizione;
    }

    @Override
    public String toString() {
        return "Products{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", descrizione='" + descrizione + '\'' +
                '}';
    }
}
