package com.example.magazzinoservice.Controllers;


import com.example.magazzinoservice.Entity.Products;
import com.example.magazzinoservice.Service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("api")
public class ProductsController {

    @Autowired
    private ProductsService productsService;


    //API CHE OTTIENE TUTTI GLI ARTICOLI
    @GetMapping(value="/all",produces="application/json")
    public ResponseEntity<Object> FindAllArticoli(){
        try {
            Iterable<Products> ArticoliList;
            ArticoliList=productsService.SelTutti();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("message", "Elementi ottenuti con successo");
            map.put("status", HttpStatus.OK);
            map.put("result", ArticoliList);
            return new ResponseEntity<Object>(map,HttpStatus.OK);
        }catch(Exception e) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("message", "Elementi ottenuti con successo");
            map.put("status", HttpStatus.BAD_REQUEST);
            map.put("result",e.getMessage());
            return new ResponseEntity<Object>(map,HttpStatus.OK);
        }
    }


    //API CHE OTTIENE TUTTI GLI ARTICOLI
    @GetMapping(value="/{id}",produces="application/json")
    public ResponseEntity<Object> Find(@PathVariable("id") UUID id){
        try {
            Products  articolo;
            articolo=productsService.SelSingle(id);
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("message", "Elementi ottenuti con successo");
            map.put("status", HttpStatus.OK);
            map.put("result", articolo);
            return new ResponseEntity<Object>(map,HttpStatus.OK);
        }catch(Exception e) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("message", "Elementi ottenuti con successo");
            map.put("status", HttpStatus.BAD_REQUEST);
            map.put("result",e.getMessage());
            return new ResponseEntity<Object>(map,HttpStatus.OK);
        }
    }

    @PostMapping(value="/store",produces="application/json")
    public ResponseEntity<Object> Find(@RequestBody Products articolo){
        try {
           productsService.insArticolo(articolo);
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("message", "Elementi ottenuti con successo");
            map.put("status", HttpStatus.OK);
            map.put("result", articolo);
            return new ResponseEntity<Object>(map,HttpStatus.OK);
        }catch(Exception e) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("status", HttpStatus.BAD_REQUEST);
            map.put("result",e.getMessage());
            return new ResponseEntity<Object>(map,HttpStatus.OK);
        }
    }


    @PostMapping(value="/edit/{id}",produces="application/json")
    public ResponseEntity<Object> Find(@RequestBody Products articolo,@PathVariable ("id") UUID id){
        try {
            Products articolo_update=productsService.updateArticolo(articolo,id);
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("message", "Elementi ottenuti con successo");
            map.put("status", HttpStatus.OK);
            map.put("result", articolo);
            return new ResponseEntity<Object>(map,HttpStatus.OK);
        }catch(Exception e) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("message", "Elementi ottenuti con successo");
            map.put("status", HttpStatus.BAD_REQUEST);
            map.put("result",e.getMessage());
            return new ResponseEntity<Object>(map,HttpStatus.OK);
        }
    }


    @DeleteMapping(value="/delete/{id}",produces="application/json")
    public ResponseEntity<Object> delete(@PathVariable("id") UUID id){
        try {
            productsService.DelArticolo(id);
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("message", "Elemento eliminato con successo");
            map.put("status", HttpStatus.OK);
            map.put("result", null);
            return new ResponseEntity<Object>(map,HttpStatus.OK);
        }catch(Exception e) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("message", "Elementi ottenuti con successo");
            map.put("status", HttpStatus.BAD_REQUEST);
            map.put("result",e.getMessage());
            return new ResponseEntity<Object>(map,HttpStatus.OK);
        }
    }





}
