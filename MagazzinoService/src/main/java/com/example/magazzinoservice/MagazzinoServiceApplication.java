package com.example.magazzinoservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MagazzinoServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MagazzinoServiceApplication.class, args);
    }

}
