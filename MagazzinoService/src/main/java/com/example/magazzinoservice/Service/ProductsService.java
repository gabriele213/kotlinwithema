package com.example.magazzinoservice.Service;

import com.example.magazzinoservice.Entity.Products;
import com.example.magazzinoservice.Repository.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class ProductsService {

    @Autowired
    ProductsRepository productsRepository;

    public Iterable<Products> SelTutti() {
        return productsRepository.findAll();
    }

    public List<Products> SelTuttiPage(PageRequest page) {
        return productsRepository.findAll(page).getContent();
    }


    public Products SelSingle(UUID codArt) {
        // TODO Auto-generated method stub
        return productsRepository.findById(codArt).get();
    }

    // Update operation
    @Transactional
    public Products updateArticolo(Products articolo,UUID codArt) {
        Products articoloDb=productsRepository.findById(codArt).get();
        articoloDb.setUser_id(articolo.getUser_id());
        articoloDb.setDescrizione(articolo.getDescrizione());
        return productsRepository.save(articoloDb);
    }


    //DELETE OPERAZIONE//
    @Transactional
    public void DelArticolo(UUID codArt) {
        Products articoloDb=productsRepository.findById(codArt).get();
        productsRepository.delete(articoloDb);
    }
    //INSERIMENTO//
    @Transactional
    public Products insArticolo(Products articolo) {
        articolo=productsRepository.save(articolo);
        return articolo;
    }

}
