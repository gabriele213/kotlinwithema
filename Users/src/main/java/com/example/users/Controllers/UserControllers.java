package com.example.users.Controllers;

import com.example.users.Models.Entity.RabbitMessage.Message;
import com.example.users.Models.Entity.User;
import com.example.users.Models.Response.ResponseHandler;
import com.example.users.Models.ResponseSchema.UserListResponse;
import com.example.users.Models.ResponseSchema.UserResponse;
import com.example.users.Service.RabbitMq.MQConfig;
import com.example.users.Service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.users.Models.Response.ApiResponse.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("api/user")
public class UserControllers {

    @Autowired
    private UserService userService;

    @Autowired
    private RabbitTemplate template;


    @Operation(summary = "Find User by UUID", description = "Find User by UUID ", tags = { "user-controllers" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully find data",
                    content = @Content(schema = @Schema(implementation = UserResponse.class))) })
    @GetMapping("/{id}")
    public ResponseEntity<Object> getUser(@PathVariable("id") UUID id){
        try {
            Optional<User> res = Optional.ofNullable(this.userService.get(id));
            if (res.isPresent()) {
                // Messages sending logic//
               Message msg= new Message();
               msg.setDescription("ciao");
               msg.setMessage("ciao");
               System.out.println(msg);
                this.publishMessage(msg);
                //END SENDING MESSAGGIO//
                SuccessResponse<Object> responseObject=new SuccessResponse<>(res.get(),"Utente recuperato con successo");
                return ResponseHandler.sendResponse(responseObject, HttpStatus.OK);
            }else{
                ErrorResponse responseObject=new ErrorResponse("User not found","Utente non trovato");
                return ResponseHandler.sendErrorResponse(
                        responseObject,
                        HttpStatus.BAD_REQUEST
                );
            }
        } catch (Exception e) {
            return ResponseHandler.unexpectedErrorResponse(e.getMessage().toString());
        }
    }

    @PostMapping("/store")
    public User addUser(@RequestBody  User user) {
        return this.userService.save(user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id")  UUID id){
       this.userService.delete(id);
    }


    @PutMapping("/{id}")
    public void UpdateUser(@PathVariable("id")  UUID id,User user){
        this.userService.update(id,user);
    }


    @Operation(summary = "Find List of Users", description = "Find List Users ", tags = { "user-controllers" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully find data",
                    content = @Content(schema = @Schema(implementation = UserListResponse.class))) })
    @GetMapping("/dataTable")
    public ResponseEntity<Object> getDataTable(@RequestParam(defaultValue = "0") int pageNo,
                     @RequestParam(defaultValue = "10") int pageSize,
                     @RequestParam(defaultValue = "null") String filterUsername,
                     @RequestParam(defaultValue = "id") String sorted) {
        try {
            PageRequest paging = PageRequest.of(pageNo, pageSize);
            int total = userService.getAll().size();
            List<User> list=null;
            if(!Objects.equals(filterUsername, "null")){
                list= userService.getAllPagingFilterUsernameSort(filterUsername,paging,sorted);
            }else{
                list=userService.getAllPaging(paging);
            }
            PaginationMetadata pageObject= new PaginationMetadata(pageNo,pageSize,0,total,null);
            SuccessResponse<Object> responseObject=new SuccessResponse<>(list,pageObject,"Utenti recuperato con successo");
            return ResponseHandler.sendResponse(responseObject, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseHandler.unexpectedErrorResponse(e.getMessage().toString());
        }
    }

    private void publishMessage(Message log){
        template.convertAndSend(MQConfig.exchangeName, MQConfig.routingKey, log);
        System.out.println("MESSAGGIO MANDATO CON SUCCESSO");
    }


}
