package com.example.users.Controllers;

import com.example.users.Service.RabbitMq.RabbitMqSync;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class RabbitAsyncControllers {

        @Autowired
        private RabbitTemplate rabbitTemplate;

        @PostMapping("/send")
        public String send(@RequestParam ("message") String message) {
            Message newMessage = MessageBuilder.withBody(message.getBytes()).build();
            System.out.println("HO MANDATO"+message);
            Message result = rabbitTemplate.sendAndReceive(RabbitMqSync.RPC_EXCHANGE, RabbitMqSync.RPC_QUEUE1, newMessage);
            String response = "";
            if (result != null) {
                    response = new String(result.getBody());
                    System.out.println("HO RICEVUTO"+response);
                }
            return response;
        }
}
