package com.example.users.Models.Response;

public class ApiResponse {
    static class PaginationLink {
        /**
         * Compose url for the first page
         */
       public String first=null;

        /**
         * Compose url for the last page
         */
        public String last=null;

        /**
         * Compose url for the next page
         */
        public String next=null;
        /**
         * Compose url for tge prev page
         */
        public String prev=null;

        PaginationLink(String first, String last,String next,String prev) {
            this.first = first;
            this.last = last;
            this.next = next;
            this.prev=prev;
        }
    }

    public static  class PaginationMetadata {
        /**
         * Current page number
         */
        public int page_number=0;

        /**
         * Current page size
         */
        public int page_size=0;

        /**
         * Number of item in the current data object
         */
        public int page_count=0;

        /**
         * Number of item in the storage
         */
        public int total_count=0;

        /**
         * Link for next and prev pagination
         */
        PaginationLink link=null;

        public PaginationMetadata(int page_number, int page_size, int page_count, int total_count, PaginationLink link) {
            this.page_number = page_number;
            this.page_size = page_size;
            this.page_count = page_count;
            this.total_count=total_count;
            this.link=link;
        }

    }

    public static class SuccessResponse<T> {

        /**
         * Object that contains the data
         */
        public T data=null;

        /**
         * Link for next and prev pagination
         */
        public PaginationMetadata pagination=null;

        /**
         * Human redeable message
         */
        public String message="No message provided";


        public SuccessResponse(T data, String message) {
            this.data = data;
            this.message = message;
        }

        public SuccessResponse(T data, PaginationMetadata pagination, String message) {
            this.data = data;
            this.pagination = pagination;
            this.message = message;
        }
    }

    public static class ErrorResponse {

        /**
         * Error code identifier
         */
        String error = "Unkown error";

        /**
         * Human redeable message
         */
        String message = "No message provided";

        /**
         * Object with detailed error
         */
        Object details = null;

        public ErrorResponse(String error, String message) {
            this.error = error;
            this.message = message;
        }

        ErrorResponse(String error, String message, Object details) {
            this.error = error;
            this.message = message;
            this.details = details;
        }
    }
}
