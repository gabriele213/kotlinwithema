package com.example.users.Models.Response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ResponseHandler {

    public static ResponseEntity<Object> sendResponse(ApiResponse.SuccessResponse<Object> responseObject, HttpStatus status) {
        return new ResponseEntity(responseObject, status);
    }

    public static ResponseEntity<Object> sendErrorResponse(ApiResponse.ErrorResponse responseObject, HttpStatus status){
        return  new ResponseEntity(responseObject, status);
    }

    public static ResponseEntity<Object> unexpectedErrorResponse(String message){
        String error = "Internal error";
       HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ApiResponse.ErrorResponse responseObject = new ApiResponse.ErrorResponse(error,message,status);
        return new ResponseEntity(responseObject, status);
    }
}
