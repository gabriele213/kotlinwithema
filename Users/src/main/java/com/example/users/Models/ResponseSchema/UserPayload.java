package com.example.users.Models.ResponseSchema;

import java.util.UUID;

public class UserPayload {
    public UUID id;
    public String username;

    public String email;

    UserPayload() {
        this.id = UUID.randomUUID();
        this.username = "example@example.com";
        this.email = "example@example.com";
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
