package com.example.users.Models.Entity;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import  javax.persistence.Entity;

import java.util.UUID;


@Entity
@Table(name="Users")
public class User {
    @Id
    @Column(name="id")
    @GeneratedValue
    @Type(type="org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name="username",nullable=false)
    private String username;
    @Column(name="email",nullable=false)
    private String email;
    @Column(name="password",nullable=false)
    private String password;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User(UUID id, String username, String email, String password) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public User(){
        super();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
