package com.example.users.Models.Entity.RabbitMessage;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.io.Serializable;


public class Message {
    @JsonProperty("message")
    public String message;
    @JsonProperty("description")
    public String  description;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Message{" +
                "message='" + message + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
    public Message(String message, String description) {
        this.message = message;
        this.description = description;
    }

    public  Message(){
        super();
    }
}
