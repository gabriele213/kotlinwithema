package com.example.users.Models.ResponseSchema;

import com.example.users.Models.Response.ApiResponse;

import java.util.ArrayList;
import java.util.List;

public class UserListResponse {
    List<UserPayload> data;
    ApiResponse.PaginationMetadata pagination;
    String message;

    public List<UserPayload> getData() {
        return data;
    }

    public void setData(List<UserPayload> data) {
        this.data = data;
    }

    public ApiResponse.PaginationMetadata getPagination() {
        return pagination;
    }

    public void setPagination(ApiResponse.PaginationMetadata pagination) {
        this.pagination = pagination;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "UserListResponse{" +
                "data=" + data +
                ", pagination=" + pagination +
                ", message='" + message + '\'' +
                '}';
    }

    public UserListResponse(List<UserPayload> data, ApiResponse.PaginationMetadata pagination, String message) {
        this.data = data;
        this.pagination = pagination;
        this.message = "No message";
    }
}
