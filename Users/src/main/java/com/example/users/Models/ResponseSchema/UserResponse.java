package com.example.users.Models.ResponseSchema;

import com.example.users.Models.Response.ApiResponse;

import java.util.ArrayList;
import java.util.List;

public class UserResponse{
       public  UserPayload data;
        public ApiResponse.PaginationMetadata pagination;
        public String message;

       public  UserResponse(UserPayload data, ApiResponse.PaginationMetadata pagination,String message) {
            this.data=data;
            this.pagination=pagination;
            this.message="No message";
        }

    }

