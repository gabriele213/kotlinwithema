package com.example.users.Service.RabbitMq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqSync {
    public static final String RPC_QUEUE1 = "outbound-queue";
    public static final String RPC_QUEUE2 = "inbound-queue";
    public static final String RPC_EXCHANGE = "rpc_exchange";

        @Bean
        Queue msgQueue() {
            return new Queue(RPC_QUEUE1);
        }

        @Bean
        Queue replyQueue() {
            return new Queue(RPC_QUEUE2);
        }

        @Bean
        TopicExchange exchangeSync() {
            return new TopicExchange(RPC_EXCHANGE);
        }


        @Bean
        Binding msgBinding() {
            return BindingBuilder.bind(msgQueue()).to(exchangeSync()).with(RPC_QUEUE1);
        }


        @Bean
        Binding replyBinding() {
            return BindingBuilder.bind(replyQueue()).to(exchangeSync()).with(RPC_QUEUE2);
        }

        @Bean
        RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
            RabbitTemplate template = new RabbitTemplate(connectionFactory);
            template.setReplyAddress(RPC_QUEUE2);
            template.setReplyTimeout(6000);
            return template;
        }



        @Bean
        SimpleMessageListenerContainer replyContainer(ConnectionFactory connectionFactory) {
            SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
            container.setConnectionFactory(connectionFactory);
            container.setQueueNames(RPC_QUEUE2);
            container.setMessageListener(rabbitTemplate(connectionFactory));
            return container;
        }
}
