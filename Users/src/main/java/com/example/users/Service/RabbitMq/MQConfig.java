package com.example.users.Service.RabbitMq;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.DefaultClassMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;

import javax.annotation.Resource;

@Configuration
public class MQConfig  implements InitializingBean {
    public static final String queueName= "system-queue";
    public static final String exchangeName = "system-exchange";
    public  static final String routingKey="system-routing";

    @Resource
    public RabbitTemplate rabbitTemplate;


    @Bean
    public Queue queue(){
        return new Queue(queueName);
    }

    //Make exchange
    @Bean
    public TopicExchange exchange(){
        return new TopicExchange(exchangeName);
    }

    //Bind queue to exchange
    @Bean
    public Binding binding(Queue queue,TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(routingKey);
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        Jackson2JsonMessageConverter messageConverter = new Jackson2JsonMessageConverter();
        DefaultClassMapper defaultClassMapper = new DefaultClassMapper();
        defaultClassMapper.setDefaultType(Message.class);
        messageConverter.setClassMapper(defaultClassMapper);
        rabbitTemplate.setMessageConverter(messageConverter);
    }



}
