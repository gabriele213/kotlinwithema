package com.example.users.Service;

import com.example.users.Models.Entity.User;
import com.example.users.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class UserService {
    @Autowired
    UserRepository userRepository;



    public List<User>getAll(){
        return userRepository.findAll();
    }

    public List<User> getAllPaging(PageRequest page){
        return  userRepository.findAll(page).getContent();
    }
    public List<User> getAllPagingFilterUsernameSort(String username,PageRequest page,String sort){
        return userRepository.findByUsernameContainingIgnoreCase(username,page, Sort.by(sort));
    }
    public User get(UUID id){
        return  userRepository.findById(id).get();
    }

    @Transactional
    public Boolean delete(UUID id){
        Optional<User> userData = userRepository.findById(id);
        if (userData.isPresent()) {
            userRepository.delete(userData.get());
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public User save(User user){
        return  userRepository.save(user);
    }

    @Transactional
    public User update(UUID ID,User user){
        Optional<User> userData = userRepository.findById(ID);
        if(userData.isPresent()){
            userData.get().setUsername(user.getUsername());
            userData.get().setEmail(user.getEmail());
            userData.get().setPassword(user.getPassword());
            return userRepository.save(userData.get());
        }else{
            return null;
        }
    }










}
