package com.example.users.Repository;

import com.example.users.Models.Entity.User;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    public List<User> findByUsernameContainingIgnoreCase(String username, PageRequest pageable, Sort sort) ;

}
