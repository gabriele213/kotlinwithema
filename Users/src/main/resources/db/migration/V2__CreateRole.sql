CREATE TABLE Roles (
                       Id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
                       name varchar(255) NOT NULL,
                       description varchar(255)
)