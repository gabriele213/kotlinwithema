CREATE TABLE User_Role (
                           Id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
                           user_id uuid NOT NULL,
                           role_id uuid NOT NULL,
                           CONSTRAINT fk_user
                               FOREIGN KEY(user_id)
                                   REFERENCES Users(Id),
                           CONSTRAINT fk_role
                               FOREIGN KEY(role_id)
                                   REFERENCES Roles(Id)
)