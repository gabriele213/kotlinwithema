CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE Users (
                       Id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
                       username varchar(255) NOT NULL,
                       email varchar(255) NOT NULL,
                       password varchar(255) NOT NULL
)
