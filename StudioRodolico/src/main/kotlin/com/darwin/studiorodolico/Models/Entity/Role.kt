package com.darwin.studiorodolico.Models.Entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
@Table(name="Roles")
class Role (
    @Column(nullable = false)
    @Id
    @Type(type="org.hibernate.type.PostgresUUIDType")
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private var id: UUID,
    @Column(name="name",nullable=false)
    private var name: String,
    @Column(name="description",nullable=true)
    private var description: String,
){

    @ManyToMany(mappedBy="role")
    @JsonIgnore
    private var users: List<User> = listOf()

    fun getUser(): List<User> {
        return this.users;
    }
}