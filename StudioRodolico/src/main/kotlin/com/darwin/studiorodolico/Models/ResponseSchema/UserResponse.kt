package com.darwin.studiorodolico.Models.ResponseSchema

import com.darwin.studiorodolico.Models.Entity.User
import com.darwin.studiorodolico.Models.Response.PaginationMetadata
import java.util.*
import javax.persistence.Column

class UserPayload {
     var id: UUID
     var username: String
     var email: String
     constructor(){
         this.id = UUID.randomUUID()
         this.username = "example@example.com"
         this.email = "example@example.com"
     }
}

class UserResponse<Any> {
    var data : UserPayload? = UserPayload()
    var pagination: PaginationMetadata? = null
    var message: String = "No message provided"
}

class UserListPayload {
    var data: List<UserPayload>? = ArrayList<UserPayload>()
    var pagination: PaginationMetadata? = null
    var message: String = "No message provided"
}
