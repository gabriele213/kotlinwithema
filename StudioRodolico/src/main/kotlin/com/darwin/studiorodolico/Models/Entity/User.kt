package com.darwin.studiorodolico.Models.Entity

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

@Entity
@Table(name="Users")
class User(
    @Column(nullable = false)
    @Id
    @Type(type="org.hibernate.type.PostgresUUIDType")
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private var id: UUID,
    @Column(name="username",nullable=false)
    private var username: String,
    @Column(name="email",nullable=false)
    private var email: String,
    @Column(name="password",nullable=false)
    private var password: String,
) {
    @ManyToMany
    @JoinTable(name = "user_role",
        joinColumns = arrayOf(JoinColumn(name = "user_id")),
        inverseJoinColumns = arrayOf(JoinColumn(name = "role_id", referencedColumnName = "id"))
    )
    private var role: List<Role> = listOf()

    fun getId(): UUID {
        return this.id;
    }
    fun setId(value:UUID){
        this.id=value;
    }

    fun getUsername(): String {
        return this.username;
    }
    fun setUsername(value:String){
        this.username=value;
    }

    fun getEmail(): String {
        return this.email;
    }
    fun setEmail(email:String){
        this.email =email;
    }

    fun getPassword(): String {
        return this.password;
    }
    fun setPassword(password: String){
        this.password=password;
    }

    fun getRole(): List<Role> {
        return this.role;
    }
}