package com.darwin.studiorodolico.Models.Entity.RabbitMessage

import org.springframework.stereotype.Component
import java.io.Serializable

class SystemLog(
    var message: String,
    var description: String? = null) : Serializable {}