package com.darwin.studiorodolico.Models.Response

import java.util.Objects

class PaginationLink {
    /**
     * Compose url for the first page
     */
    var first: String? = null

    /**
     * Compose url for the last page
     */
    var last: String? = null

    /**
     * Compose url for the next page
     */
    var next: String? = null

    /**
     * Compose url for tge prev page
     */
    var prev: String? = null

    constructor(first: String, last:String,next:String,prev:String) {
        this.first = first
        this.last = last
        this.next = next
        this.prev=prev
    }
}

class PaginationMetadata {
    /**
     * Current page number
     */
    var page_number: Int? = null

    /**
     * Current page size
     */
    var page_size: Int? = null

    /**
     * Number of item in the current data object
     */
    var page_count: Int? = null

    /**
     * Number of item in the storage
     */
    var total_count: Int? = null

    /**
     * Link for next and prev pagination
     */
    var link: PaginationLink? = null

    constructor(page_number: Int?, page_size:Int?,page_count:Int?,total_count:Int?,link:PaginationLink?) {
        this.page_number = page_number
        this.page_size = page_size
        this.page_count = page_count
        this.total_count=total_count
        this.link=link
    }

}

class SuccessResponse<T> {

    /**
     * Object that contains the data
     */
    var data: T? = null

    /**
     * Link for next and prev pagination
     */
    var pagination: PaginationMetadata? = null

    /**
     * Human redeable message
     */
    var message: String = "No message provided"
    constructor(message: String) {
        this.message = message
    }

    constructor(data: T, message: String) {
        this.data = data
        this.message = message
    }

    constructor(data: T, pagination: PaginationMetadata, message: String) {
        this.data = data
        this.pagination = pagination
        this.message = message
    }
}

class ErrorResponse {

    /**
     * Error code identifier
     */
    var error: String = "Unknown error"

    /**
     * Human redeable message
     */
    var message: String = "No message provided"

    /**
     * Object with detailed error
     */
    var details: Objects? = null

    constructor(error: String, message: String) {
        this.error = error
        this.message = message
    }

    constructor(error: String, message: String, details: Objects?) {
        this.error = error
        this.message = message
        this.details = details
    }
}