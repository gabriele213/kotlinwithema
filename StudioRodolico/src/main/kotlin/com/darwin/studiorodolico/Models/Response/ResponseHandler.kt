package com.darwin.studiorodolico.Models.Response

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.util.*


@RestControllerAdvice
class ResponseHandler {
    fun sendResponse(responseObject: SuccessResponse<Any>, status: HttpStatus): ResponseEntity<Any> {
        return ResponseEntity(responseObject, status)
    }

    fun sendErrorResponse(responseObject: ErrorResponse, status: HttpStatus): ResponseEntity<Any>{
        return ResponseEntity(responseObject, status)
    }

    fun unexpectedErrorResponse(message: String, error: String = "Internal error" ,status: HttpStatus = HttpStatus.INTERNAL_SERVER_ERROR): ResponseEntity<Any>{
        var responseObject: ErrorResponse = ErrorResponse(error, message);
        return ResponseEntity(responseObject, status)
    }
}