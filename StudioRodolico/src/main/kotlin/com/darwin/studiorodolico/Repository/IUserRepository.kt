package com.darwin.studiorodolico.Repository

import com.darwin.studiorodolico.Models.Entity.User
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*


interface IUserRepository : JpaRepository<User, UUID>{

    //1 METODO TRAMITE QUERY SQL IN BASE AL PARAMETRO//
   // @Query(
     //   value = "select * from user where username LIKE '%'+:username+'%'",
       // nativeQuery = true
    //)
    //fun SelByUsernameLikeQuery(@Param("param") username: String?): ArrayList<User?>?


    //2 METODO CHE FA UNA RICERCA TRAMITE LIKE USERNAME E UNA PAGINATION//
    fun findByUsernameContainingIgnoreCase(username: String, pageable: PageRequest, sort:Sort): List<User?>?


}