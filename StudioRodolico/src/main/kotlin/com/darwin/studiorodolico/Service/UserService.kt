package com.darwin.studiorodolico.Service

import com.darwin.studiorodolico.Models.Entity.User
import com.darwin.studiorodolico.Repository.IUserRepository
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
@Transactional()
class UserService (val UserRepository: IUserRepository){

    @Transactional
    fun getAll(): List<User>{
        return UserRepository.findAll();
    }

    @Transactional
    fun getAllPaging(page: PageRequest): List<User>{
        return UserRepository.findAll(page).content;
    }

    @Transactional
    fun getAllPagingFilterUsernameSort(username:String,page: PageRequest,SortParam:String): List<User?>? {
        return UserRepository.findByUsernameContainingIgnoreCase(username,page,Sort.by(SortParam));
    }


    @Transactional
    fun get(id: UUID): Optional<User>{
        println(id.toString())
        return UserRepository.findById(id);
    }

    @Transactional
    fun save(user: User): User {
        return UserRepository.save(user);
    }

    @Transactional
    fun update(id: UUID, user: User): User? {
        val userData: Optional<User> = UserRepository.findById(id);
        if(userData.isPresent){
            userData.get().setUsername(user.getUsername())
            userData.get().setEmail(user.getEmail())
            userData.get().setPassword(user.getPassword())
            return UserRepository.save(userData.get())
        }else{
            return null;
        }
    }

    @Transactional
    fun delete(id: UUID): Boolean {
        val userData: Optional<User> = UserRepository.findById(id);
        if (userData.isPresent) {
            UserRepository.delete(userData.get())
            return true;
        } else {
            return false;
        }
    }
}