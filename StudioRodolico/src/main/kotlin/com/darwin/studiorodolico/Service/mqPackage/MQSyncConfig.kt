package com.darwin.studiorodolico.Service.mqPackage

import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.TopicExchange
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MQSyncConfig {

    //TODO: https://docs.spring.io/spring-amqp/docs/current/reference/html/#reference
    //Reference : https://www.springcloud.io/post/2022-01/springboot-rabbitmq-rpc/#gsc.tab=0

    companion object {
        val queueOutName: String = "outbound-queue"
        val queueInName: String = "inbound-queue"
        val rpcExchange: String = "rpc_exchange"
        val replyTimeout: Long = 30000
    }

    /*
        This configuration will allow the microservice to synchronously
        comunicate with another microservice. This message will be send
        through 2 different queues. Message and Response
     */

    //Outbound queue (necessary to send request)
    @Bean
    fun queueOut(): Queue{
        return Queue(queueOutName);
    }

    //Inbound queue (necessary to receive response)
    @Bean
    fun queueIn(): Queue{
        return Queue(queueInName);
    }

    @Bean
    fun exchangeSync(): TopicExchange{
        return TopicExchange(rpcExchange)
    }

    @Bean
    fun outboundQueueBinding(@Qualifier("queueOut") outQueue: Queue, @Qualifier("exchangeSync") exchange : TopicExchange): Binding {
        return BindingBuilder.bind(outQueue).to(exchange).with(queueOutName);
    }

    @Bean
    fun inboundQueueBinding(@Qualifier("queueIn") inQueue: Queue, @Qualifier("exchangeSync") exchange : TopicExchange): Binding {
        return BindingBuilder.bind(inQueue).to(exchange).with(queueInName);
    }

    /*
        This configuration allow the comunication without limitation on the object type
     */
    @Bean
    fun syncMsessageConverter(): MessageConverter {
        return Jackson2JsonMessageConverter();
    }


    @Bean
    fun rabbitTemplate(connectionFactory: ConnectionFactory) : RabbitTemplate{
        val template = RabbitTemplate(connectionFactory);
        template.setMessageConverter(syncMsessageConverter());
        template.setReplyAddress(queueInName);
        template.setReplyTimeout(replyTimeout);
        return template;
    }

    @Bean
    fun replyContainer(connectionFactory: ConnectionFactory) : SimpleMessageListenerContainer{
        val container = SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueInName);
        container.setMessageListener(rabbitTemplate(connectionFactory));
        return container;
    }
}