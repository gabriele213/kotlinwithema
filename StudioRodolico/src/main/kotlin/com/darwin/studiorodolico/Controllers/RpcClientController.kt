package com.darwin.studiorodolico.Controllers

import com.darwin.studiorodolico.Service.mqPackage.MQSyncConfig
import org.hibernate.annotations.GeneratorType
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageBuilder
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin
@RestController
@RequestMapping("/api/")
class RpcClientController (
    @Autowired
    private val rabbitTemplate: RabbitTemplate
){
    @PostMapping("/send")
    fun publish() : ResponseEntity<Any> {
        val messageString = "Message sent to Rabbit";
        println(messageString);
        val message : Message = MessageBuilder.withBody(messageString.encodeToByteArray()).build();
        val result : Message? = rabbitTemplate.sendAndReceive(MQSyncConfig.rpcExchange, MQSyncConfig.queueOutName, message);
        if(result != null) {
             println("MESSAGGIO RITORNATO");
             println(String(result.getBody()));
            return ResponseEntity.ok().build();
        }else{
            println("ERRORE");
            return ResponseEntity.noContent().build();
        }
    }
}