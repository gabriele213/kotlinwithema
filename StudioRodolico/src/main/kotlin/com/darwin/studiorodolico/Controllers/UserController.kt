package com.darwin.studiorodolico.Controllers

import com.darwin.studiorodolico.Models.Entity.RabbitMessage.SystemLog
import com.darwin.studiorodolico.Models.Entity.User
import com.darwin.studiorodolico.Models.Response.*
import com.darwin.studiorodolico.Models.ResponseSchema.UserListPayload
import com.darwin.studiorodolico.Models.ResponseSchema.UserResponse
import com.darwin.studiorodolico.Service.UserService
import com.darwin.studiorodolico.Service.mqPackage.MQConfig
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*


@CrossOrigin
@RestController
@RequestMapping("api/user")
class UserController(
    @Autowired
    private val userService: UserService,
    private val ResponseHandler: ResponseHandler,
    @Autowired
    private val template: RabbitTemplate
) {

    @Operation(summary = "Search User from id", description = "Seach User from id")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "Successful Operation",
                content = [Content(
                    mediaType = "application/json",
                    schema = Schema(implementation = UserResponse::class)
                )]
            ),
            ApiResponse(
                responseCode = "400", description = "Error Operation",
                content = [Content(
                    mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class)
                )]
            ),
        ]
    )
    @GetMapping("/{id}")
    fun getUser(@PathVariable("id") id: UUID): ResponseEntity<User>? {
        try {
            val res = this.userService.get(id);
            if (res.isPresent) {
                //Messages sending logic
                val log: SystemLog = SystemLog("ciao", "ciao")
                this.publishMessage(log);
                val responseObject: SuccessResponse<Any> = SuccessResponse(res.get(), "Utente recuperato con successo");
                return this.ResponseHandler.sendResponse(responseObject, HttpStatus.OK) as ResponseEntity<User>
            }

            val responseObject: ErrorResponse = ErrorResponse("User not found", "Utente non trovato");
            return this.ResponseHandler.sendErrorResponse(
                responseObject,
                HttpStatus.BAD_REQUEST
            ) as ResponseEntity<User>
        } catch (e: Exception) {
            return this.ResponseHandler.unexpectedErrorResponse(e.message.toString()) as ResponseEntity<User>;
        }

    }

    @PostMapping("/store")
    fun addUser(@RequestBody user: User): User? {
        return this.userService.save(user)
    }

    @DeleteMapping("/{id}")
    fun deleteUser(@PathVariable("id") id: UUID): String {
        val operation: Boolean = this.userService.delete(id)
        return if (operation) "Eliminazione avvenuta" else "Utente non trovato"
    }


    @PutMapping("/{id}")
    fun updateUser(@PathVariable("id") id: UUID, @RequestBody user: User): User? {
        return this.userService.update(id, user)
    }

    @Operation(summary = "User datatable with filter", description = "Seach User from id")
    @ApiResponses(
        value = [
            ApiResponse(
                responseCode = "200", description = "Successful Operation",
                content = [Content(
                    mediaType = "application/json",
                    schema = Schema(implementation = UserListPayload::class)
                )]
            ),
            ApiResponse(
                responseCode = "400", description = "Error Operation",
                content = [Content(
                    mediaType = "application/json",
                    schema = Schema(implementation = ErrorResponse::class)
                )]
            ),
        ]
    )
    @GetMapping("/dataTable")
    fun getDataTable(@RequestParam(defaultValue = "0") pageNo:Int,
                     @RequestParam(defaultValue = "10") pageSize:Int,
                     @RequestParam(defaultValue = "null") filterUsername:String,
                     @RequestParam(defaultValue = "id") sorted:String): ResponseEntity<User>? {
        try {
            val paging = PageRequest.of(pageNo, pageSize)
            val total = userService.getAll().size;
            var list: List<User>? =null;
//            println(list.toString());
            if(filterUsername!="null"){
                list= userService.getAllPagingFilterUsernameSort(filterUsername,paging,sorted) as List<User>?
            }else{
                list=userService.getAllPaging(paging)
            }
            var pageObject: PaginationMetadata=PaginationMetadata(pageNo,pageSize,null,total,null);
            var responseObject: Any =
                SuccessResponse(list,pageObject, "Utenti recuperati con successo") as Any;
            return this.ResponseHandler.sendResponse(responseObject as SuccessResponse<Any>, HttpStatus.OK) as ResponseEntity<User>
        } catch (e: Exception) {
            return this.ResponseHandler.unexpectedErrorResponse(e.message.toString()) as ResponseEntity<User>;
        }
    }

    private fun publishMessage(log: SystemLog){
        template.convertAndSend(MQConfig.exchangeName, MQConfig.routingKey, log);
        println("Message published");
        return;
    }
}